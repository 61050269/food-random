export const breakfastMenus = [
  {
    name: "Bacon, egg and cheese sandwich",
    image: "/images/foods/bacon_egg_sandwich.jpg",
  },
  {
    name: "Biscuits and gravy",
    image: "/images/foods/biscuits_gravy.jpg",
  },
  {
    name: "Boiled egg",
    image: "/images/foods/boiled_egg.jpg",
  },
  {
    name: "Breakfast burrito",
    image: "/images/foods/breakfast_burrito.jpg",
  },
  {
    name: "cereal",
    image: "/images/foods/cereal.jpg",
  },
  {
    name: "Breakfast sandwich",
    image: "/images/foods/breakfast_sandwich.jpg",
  },
  {
    name: "Cinnamon roll",
    image: "/images/foods/cinnamon_roll.jpg",
  },
  {
    name: "Taco",
    image: "/images/foods/taco.jpg",
  },
  {
    name: "French toast",
    image: "/images/foods/french_toast.jpeg",
  },
  {
    name: "Fried cheese",
    image: "/images/foods/fried_cheese.jpg",
  },
  {
    name: "Fried egg",
    image: "/images/foods/fried_egg.jpg",
  },
  {
    name: "Ham and eggs",
    image: "/images/foods/ham_eggs.jpg",
  },
  {
    name: "Hamburger",
    image: "/images/foods/hamburger.jpg",
  },
  {
    name: "Muffin",
    image: "/images/foods/muffin.jpg",
  },
  {
    name: "Omelette",
    image: "/images/foods/omelette.jpg",
  },
  {
    name: "Orange juice",
    image: "/images/foods/orange_juice.jpg",
  },
  {
    name: "Pancake",
    image: "/images/foods/pancake.jpg",
  },
  {
    name: "Poached egg",
    image: "/images/foods/poached_egg.jpg",
  },
  {
    name: "Porridge",
    image: "/images/foods/porridge.jpg",
  },
  {
    name: "Yogurt",
    image: "/images/foods/yogurt.jpg",
  },
  {
    name: "Waffle",
    image: "/images/foods/waffle.jpg",
  },
];

export const lunchMenus = [
  {
    name: "Cheese Sandwiches",
    image: "/images/foods/cheese_sandwiches.jpeg",
  },
  {
    name: "Greek Salad",
    image: "/images/foods/greek_salad.jpeg",
  },
  {
    name: "Tuna Salad",
    image: "/images/foods/tuna_salad.jpg",
  },
  {
    name: "Cheese Omelette",
    image: "/images/foods/cheese_omelette.jpg",
  },
  {
    name: "Pork Kebabs",
    image: "/images/foods/pork_kebabs.jpg",
  },
  {
    name: "Spaghetti with Meat Sauce",
    image: "/images/foods/spaghetti_meat_sauce.jpg",
  },
  {
    name: "Chicken Curry",
    image: "/images/foods/chicken_curry.jpg",
  },
  {
    name: "Roast Beef",
    image: "/images/foods/roast_beef.jpg",
  },
  {
    name: "Beef Steak",
    image: "/images/foods/beef_steak.jpg",
  },
  {
    name: "Pork Shop",
    image: "/images/foods/pork_shop.jpg",
  },
  {
    name: "Cheesy Bacon Burrito",
    image: "/images/foods/cheese_burrito.jpeg",
  },
  {
    name: "Chicken Nuggets",
    image: "/images/foods/chicken_nugget.jpeg",
  },
  {
    name: "Hot Dog",
    image: "/images/foods/hot_dog.jpg",
  },
  {
    name: "Pizza",
    image: "/images/foods/pizza.jpg",
  },
  {
    name: "Sausage and Mash",
    image: "/images/foods/sausage_mash.jpg",
  },
  {
    name: "Mac and Cheese",
    image: "/images/foods/mac_cheese.jpeg",
  },
];

export const dinnerMenus = [
  {
    name: "Pasta",
    image: "/images/foods/pasta.jpg",
  },
  {
    name: "Fire Rice",
    image: "/images/foods/fire_rice.jpg",
  },
  {
    name: "Steak",
    image: "/images/foods/dinner_steak.jpg",
  },
];

const foodByTime = {
  breakfastMenus,
  lunchMenus,
  dinnerMenus,
};

export default foodByTime;
