import React from "react";
import PropTypes from "prop-types";
import AppLayout from "./app/AppLayout";
import Comp from "../components";
import Navbar from "../components/Navbar";

const PageLayout = ({ title, description, children }) => (
  <div>
    <AppLayout title={title} description={description}>
      <Navbar />
      <main className="container">{children}</main>
    </AppLayout>
    <Comp.Footer />
  </div>
);

PageLayout.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  children: PropTypes.node,
};

PageLayout.defaultProps = {
  title: "",
  description: "",
  children: "",
};

export default PageLayout;
