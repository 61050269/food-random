/* eslint-disable import/no-cycle */
/* eslint-disable no-console */
import React, { Component } from "react";
import { Button } from "antd";
import getDateTime from "../../utils/day";
import { breakfastMenus, lunchMenus, dinnerMenus } from "../../data/foods";
import Comp from "..";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      food: "",
      loading: false,
    };
  }

  selectDate() {
    const currentTime = getDateTime();
    let food;
    this.setState({
      loading: true,
      food: "Hello",
    });
    console.log(currentTime);
    setTimeout(() => {
      switch (currentTime) {
        case "morning":
          food = breakfastMenus[Math.floor(Math.random() * breakfastMenus.length)];
          this.setState({
            food,
            loading: false,
          });
          break;
        case "noon":
        case "afternoon":
          food = lunchMenus[Math.floor(Math.random() * lunchMenus.length)];
          console.log(food);
          this.setState({
            food,
            loading: false,
          });
          break;
        case "evening":
          food = dinnerMenus[Math.floor(Math.random() * dinnerMenus.length)];
          this.setState({
            food,
            loading: false,
          });
          break;
        default:
          this.setState({
            loading: false,
          });
          break;
      }
    }, 1000);
  }

  render() {
    const { food, loading } = this.state;
    console.log(food, loading);
    return (
      <div>
        <p className="text-center">
          What i can eat in the
          {" "}
          <strong>{getDateTime()}</strong>
        </p>
        <br />
        <Button
          type="primary"
          style={{ width: "100%" }}
          onClick={() => this.selectDate()}
          disabled={loading}
        >
          {loading ? "Finding..." : "Foodom It"}
        </Button>
        {food && (
          <Comp.FoodImage foodImage={food.image} foodName={food.name} />
        )}
      </div>
    );
  }
}
