import Footer from "./Footer";
import FoodImage from "./FoodImage";
import Home from "./Home";

export default {
  Footer,
  FoodImage,
  Home,
};
