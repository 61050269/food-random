import React from "react";
import { Layout } from "antd";

const { Header } = Layout;

const Navbar = () => (
  <Header className="p-0 header-navbar">
    <div className="container navbar-wrapper">
      <div>
        <img src="/foodom.png" alt="foodom-logo" className="foodom-logo" />
        <strong className="mr-2">Foodom</strong>
      </div>
      <div>
        No idea what to eat. Let&apos;s
        {" "}
        <strong>Foodom</strong>
        {" "}
        help you.
      </div>
    </div>
  </Header>
);

export default Navbar;
