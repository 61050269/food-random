import React from "react";
import PageLayout from "../containers/PageLayout";
import Comp from "../components";
import getDateTime from "../utils/day";

export default () => (
  <PageLayout
    title={`Good 
  ${getDateTime()}`}
    description="First Page Description"
  >
    <div>
      <Comp.Home />
    </div>
  </PageLayout>
);
