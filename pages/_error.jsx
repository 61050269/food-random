import React from "react";
import PropTypes from "prop-types";
import Link from "next/link";
import PageLayout from "../containers/PageLayout";

const getErrorMessage = (statusCode) => {
  switch (statusCode) {
    case 404:
      return (
        <div className="flex flex-column error-wrapper">
          <p className="error-message">
            <strong>Error 404:</strong>
            {" "}
            Page not Found.
          </p>
          <Link href="/">
            <p className="home-page-link">Go to home page</p>
          </Link>
        </div>
      );
    case 405:
      return (
        <div className="flex flex-column error-wrapper">
          <p className="error-message">
            <strong>Error 405:</strong>
            {" "}
            Method now Allowed
          </p>
        </div>
      );
    case 502:
      return (
        <div className="flex flex-column error-wrapper">
          <p className="error-message">
            <strong>Error 502:</strong>
            {" "}
            Bad Gateway
          </p>
        </div>
      );
    default:
      return (
        <div className="flex flex-column error-wrapper">
          <p className="error-message">
            <strong>Error 500:</strong>
            {" "}
            Internal Server Error
          </p>
        </div>
      );
  }
};

const Error = ({ statusCode }) => (
  <PageLayout
    title={`Error Page ${statusCode}`}
    description={`Error page with code ${statusCode}`}
  >
    {getErrorMessage(statusCode)}
  </PageLayout>
);

Error.getInitialProps = ({ res, err }) => {
  let statusCode;
  if (res) statusCode = res.statusCode;
  else if (err) statusCode = err.statusCode;
  else statusCode = 404;
  return { statusCode };
};

Error.propTypes = {
  statusCode: PropTypes.number,
};

Error.defaultProps = {
  statusCode: 200,
};

export default Error;
