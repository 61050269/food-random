import dayjs from "dayjs";

const getDateTime = () => {
  let greeting = "";
  const currentTime = dayjs().format("HH");
  if (currentTime >= 6 && currentTime < 12) greeting = "morning";
  else if (currentTime === 12) greeting = "noon";
  else if (currentTime > 12 && currentTime < 18) greeting = "afternoon";
  else greeting = "evening";
  return greeting;
};

export default getDateTime;
